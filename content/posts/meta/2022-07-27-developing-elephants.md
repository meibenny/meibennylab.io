---
title: Developing Elephants
date: 2022-07-27T00:15:00-04:00
tags: ["meta", "elephants"]
draft: false
---

Elephants is the theme that I use for this blog. You can find its source code [here](https://gitlab.com/meibenny/elephants "Elephants Theme").

If you read the README for Elephants, you can get an idea of my development
process. Whenever I want to work on Elephants, I do it in a docker container.
Developing Elephants in a docker container has multiple advantages, most of
them are related to Docker:

- I don't need to worry about my computer's environment. Docker handles the
  environment for me every time.
- All the tools I would like to be installed are preinstalled when building
  the Docker container
- I can reliably recreate an example blog so that I can see my changes in
  near real-time on a real blog hosted entirely on my machine
- When I need to come back to working on Elephants after a long time away
  (which is what usually happens - there's a spurt of activity, and then none
  for a very long time), I can fire up the Docker container and get right back
  to work without needing to worry about setting up the right tools, binaries,
  etc.

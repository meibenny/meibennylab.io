---
title: "Informal Observations of S. speciosus Paralysis"
date: 2021-09-23T23:14:00-04:00
draft: false
---

A couple of weeks ago, specifically September 7, 2021 and September 8, 2021, we
were lucky enough to find two paralyzed cicadas. The cicada we found on
September 7 was a female cicada, most likely Neotibicen tibicen. The cicada
we found on September 8 was a male cicada, also most likely Neotibicen tibicen.

On September 7, we observed an Eastern Cicada Killer Wasp (Sphecius speciosus) 
attempting to carry the cicada back to its burrow. Luckily for us, for some
reason, the wasp dropped the cicada and didn't return for it. We moved the
cicada to the side of the road in an attempt to protect it from pedestrian
traffic - the middle of a city sidewalk is a dangerous place for a paralyzed
insect. After about six hours, we returned to where we left the cicada and saw
it was still there. We collected the cicada and brought it with us for further
study.

On September 8, we discovered what appeared to be a paralyzed cicada on the
sidewalk. We assumed that this cicada was also paralyzed by an Eastern Cicada
Killer Wasp similar to the cicada we found the day before. We took this cicada
for further study.

We kept the cicadas in a paper towel lined plastic box. To be specific, it was
a plastic box we bought from the grocery store. At first it was used to hold
mushrooms, but after we finished eating the mushroom, we recycled the box and
used it as a cicada container. We made sure the paper towel was moist to keep
the cicadas alive for as long as possible.

Later, with the help of Pavel Masek, Assistant Professor of Biological Sciences
at Binghamton University, we confirmed that the cicadas were indeed alive as
we initially suspected. Professor Masek agreed to continue observing the
cicadas in an attempt to establish a rough timeline on the length of the
paralysis caused by an Eastern Cicada Killer sting.

On September 17, 2021, Professor Masek discovered that the male cicada died.
This is about a week from when it was paralyzed by Eastern Cicada Killer.

On September 23, 2021, Professor Masek discovered that the female cicada died.
This is a little over two weeks from when it was paralyzed.

To my knowledge, this is the first time someone has written about the length of
time cicadas are paralyzed by the Eastern Cicada Killer Wasp.

REFERENCES

[1] Haspel G, Libersat F. Wasp venom blocks central cholinergic synapses to induce transient paralysis in cockroach prey. J Neurobiol. 2003 Mar;54(4):628-37. doi: 10.1002/neu.10195. PMID: 12555274.

[2] Boggs, J. (2021, September 23). Dog-Day Cicadas and Cicada Killers. Buckeye Yard & Garden onLine. https://bygl.osu.edu/node/1339.
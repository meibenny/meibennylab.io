---
title: Update On Kouign Amann
date: 2022-08-03T21:31:00-04:00
draft: false
tags:
- kouign-amann
---

This is an update on my kouign amann review on January 5, 2022. You can find it
[here][1].

I tried kougin amann at two new patisseries:

- Salon Sucre, New York, NY, USA
- Le Perche, Hudson, NY, USA

The other kouign amann I've tried before were from:

- Patisserie Au Kouign-Amann, Montreal, QC, Canada
- Patisserie Cannelle, Queens, NY, USA
- Whole Foods Market, White Plains, NY, USA
- Dominique Ansel Bakery, New York, NY, USA
- Dominique Ansel Kitchen, New York, NY, USA
- Mrs. London's Bakery, Saratoga Springs, NY, USA
- Patisserie Chanson, New York, NY, USA
- % Arabica, Brooklyn, NY, USA


Since my last post, I've identified four criteria with which to judge a kouign amann.
Each factor will be assigned a number from 1 to 5, inclusive, with 1 being the lowest
rating, and 5 being the highest rating.

## Criteria
#### Crust
The crust of a kouign amann should be firm. The crust should also glisten from
the large amounts of caramalized sugar and butter used to make the kouign amann.
The crust can be sticky from the all of the sugar.

#### Density
The interior of a kouign amann should be dense. It should be almost as dense as
a New York Style Cheesecake. However, it's also important to note that the
interior of a good kouign amann should be composed of multiple layers of pastry.

Kouign amann is not a dense cake. There should be layers of air between its 
layers of dough. The layers themselves should be very dense.

#### Flavor
A delicious kougin amann should be extremely buttery, and very sweet. Some
kougin amann have the flavor of a palmier; these kouign amann are not good kouign
amann. Generally speaking, I want to make sure that I taste the butter and sugar
in the dough. There should be ample amounts of butter and sugar.

#### Moistness
Kouign Amann should maintain a moist interior. If the inside of a kouign amann
has the consistency of a dinner roll, it's not a good kouign amann. Good kouign
amann are not rolls. Good kouign amann should be moist on the inside. The
consistency should be similar to the consistency of a moist chocolate cake. An
even closer comparison is the inside of an almond croissant from Maison Kayser.

## Rankings

| Ranking | Patisserie | Crust | Density | Flavor | Moistness |
| ------- | ---------- | ---------- | ------ | ----- | --------- |
| 1 | Patisserie Au Kouign-Amann | 5 | 5 | 5 | 5 |
| 2 | Whole Foods Market at White Plains, NY | 4 | 4 | 5 | 4 |
| 3 | Mrs. London's Bakery | 5 | 4 | 4 | 3 |
| 4 | % Arabica | 3 | 3 | 3 | 3 |
| 5 | Patisserie Cannelle | 3 | 2 | 3 | 3 |
| 6 | Le Perche | 2 | 2 | 1 | 2 | 
| 7 | Salon Sucre | 2 | 1 | 1 | 2 | 
| 8 | Dominique Ansel Bakery | 1 | 1 | 1 | 2 | 
| 8 | Dominique Ansel Kitchen | 1 | 1 | 1 | 2 | 

## Comments
#### Patisserie Au Kouign-Amann
The best kouign amann I've had in my life, ever. All other kouign amann should
be compared to the kouign amann at Patisserie Au Kouign-Amann.

#### Whole Foods Market at White Plains, NY
This is the best kouign amann I've had in the States. The exterior of the
pastry was crispy/crunchy just as I expected. The butter and sugar was layered
on, and the inside was still nice and moist. It compares very favorably to
what's offered at Patisserie Au Kouign-Amann, though it's not quite there.

#### Mrs. London's Bakery
Mrs. London's Bakery doesn't call their kouign amann a kouign amann. They opt
to call it a "Brittany". Whatever they like to call it, their version of the
kouign amann is quite stellar. It has a crispy, cruncy exterior with a soft
interior. I wish they had a little more butter and a little less sugar, but
overall it's a fantastic pastry. Considering the paltry offerings in the rest of
the States, this is close to perfection.

#### % Arabica
% Arabica is a Japanese coffee brand. Surprisingly, their version of the kouign
amann is quite good. It has the same crispy/crunchy exterior you expect of all
good kouign amann. They seem to use a little less sugar and butter, which is
good for my waistline, but unfortunately means that their kouign amann
necessarily loses to their richer competitors. Despite its drawbacks, I have no
qualms in declaring the kougin amann at % Arabica the best kouign amann in New
York City.

#### Patisserie Cannelle
I had the good fortune to try the kougin amann from Patisserie Cannelle while
visiting some friends. Their kouign amann is a flat disc shape, reminiscent of
the large kouign amann found in Patisserie Au Kouign-Amann, and similar to what
you might find in Brittany. The refreshing design is quite different from the
other kouign amann found in the states - they usually take on a cupcake form,
clearly baked for personal consumption. Patisserie Cannelle's kouign amann is
quite delicious. I enjoy the shape, and the flavors, but there's nothing that
jumps out at me as either fantastic, or a deal breaker. To be fair, I also ate
the kouign amann as the patisserie was closing, so the time the kouign amann was
sitting out on display would have definitely affected its texture and flavor
profile. Otherwise, it's quite a good option for those in New York City who want
to have a taste of what good kouign amann should be.

#### Le Perche
Le Perche came highly recommended from some friends so I had high expectations
for their food. We happened to be in Hudson, NY and stopped by for lunch, hoping
that we'd get to experience delicious food. We saw kouign amann on the menu and
we simply had to try it. It's impossible to give up an opportunity to try kouign
amann. The crust of the kouign amann was fairly firm. However it wasn't glistening
from butter or sugar. In fact, I questioned whether there was butter or sugar used
in the kouign amann at all. The interior of the kougin amann was slightly denser
than a piece of white bread. In fact, it seemed basically like a dinner roll on
the inside - though slightly moister. Its flavor is exactly the same as a palmier.

#### Salon Sucre
The kouign amann from Salon Sucre looked promising. The crust was a little sugary,
it was stickky to the touch. The crust was slightly firm, which is good. The
interior was as moist as the interior of the kouign amann from Le Perche, slightly
more moist than a dinner roll. Its flavor was similar to a palmier. Unfortunately
it wasn't as dense as I would have liked.

#### Dominque Ansel Bakery and Dominique Ansel Kitchen
I have no words for the kouign amann found at any of Dominique Ansel's properties.
They are simply not good representation of kouign amann. For what is allegedly one
of the best patisseries in New York City, the kougin amann found at Dominique
Ansel are surprisingly bad. The exterior is neither crunchy, crispy, buttery, or
sugary. The interior is dry. Overall, the kouign amann found at Dominique Ansel's
properties resemble Palmiers in both flavor and texture. Why sell kouign amann at
all if the goal is for the pastries to end up as palmiers? The kouign amann found
at Dominique Ansel's properties are an affront to kouign amann everywhere. If the
only kouign amann you've tried is at Dominique Ansel's properties, I implore you
to try kouign amann again at a better patisserie. I'm both sorry, and happy that
your first kouign amann experience was, quite possibly, the worst kouign amann
experience you've ever had.

#### Patisserie Chanson
I realize that Patisserie Chanson in my list, but remains unranked. Unfortunately,
it's been so long that I no longer recall what it was like. I'm sorry to say
that I cannot give it a rating. However, I can say that it didn't pique my
interest.

[1]: /posts/2022/01/05/on-kouign-amann/

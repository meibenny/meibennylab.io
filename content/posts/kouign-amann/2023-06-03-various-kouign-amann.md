---
title: Kougin Amann - LA to NYC
date: 2023-06-03T09:40:00-04:00
draft: false
tags:
- kouign-amann
---

I had the fortune to stop at a couple of places in Los Angeles and New York
City for more kouign amann.

In LA, I visited Coffee Commissary on Motor Avenue and Woodbine Street, and
Maru Coffee on South Sante Fe Avenue and Sacremento Street. In New York, I
braved the line at Lysee on East 21 Street and Park Avenue South for a bite
of their kouign amann.

Comparing these three kougin amann to each other, I'd give them the following
ratings:

1. Coffee Commissary
2. Maru Coffee
3. Lysee

The kouign amann from Coffee Commissary was surprisingly tasty. I was a little
hesitant at first. It was 4pm, and they only had one kouign amann still
available: a kouign amann topped with raspberry sugar. I'm always hesistant of
pastries that have flavored sugar added on top. I'm simply not a big fan of
flavored sugars. Seeing as it was my only choice, I was forced to try it. The
second I bit into the pastry, my eyes widened. The texture of the crust, the
lightness of the interior, and the sugar granules on the surface all came
together to form a delicious bite. Again, I wasn't a fan of the raspberry
flavor, but I was able to ignore it given that's what we were working with. The
deliciousness level was just superb. What was most impressive is that even at
the end of the day, the kouign amann managed to retain its moisture level. The
interior of the pastry was not dry at all. Its moisture level was something I
would expect from a freshly baked kouign amann at 7am.

Maru Coffee had a similarly delicious kouign amann. I tried it early in the
morning - around 8:30am. The kouign amann here came highly recommended from a
close friend and it did not disappoint. Like the kouign amann at Coffee
Commissary, the texture, moistness, and density all came together to form a
great pastry. There wasn't any flavored sugar here so that's a big plus. The
flavor of this kouign amann was similarly superb. My only complaint is that the
pastry has a very thick, sticky bottom. It's so sticky that it can sometimes be
stuck on your teeth and make it very difficult to actually bite through. I can
see the advantage of such a set-up. Eating this super sugary, sticky bottom
with a sip of hot coffee is heaven on earth. And let's face it, with a name like
Maru Coffee, they're in the business of selling you coffee, not just a delicious
pastry. Alas, we're here to rate kouign amann, and not coffee / kouign amann
combination meals. Because of the super sticky bottom, I need to give Coffee
Commissary the edge here.

The most disappointing of the three kouign amann I've tried is the one from
Lysee. Lysee is somewhat of a well-known bakery in New York City. I think they
were vaulted to celebrity status because of their corn dessert. It's good that
they're not known for their kouign amann. While Lysee puts out a perfectly good
pastry, it simply isn't a good kouign amann. It's entirely forgettable. If you
want a truly good, and delicious kouign amann, I'd advise you to look elsewhere.

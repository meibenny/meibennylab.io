---
title: Kougin Amann - San Francisco
date: 2023-11-20T00:40:00-05:00
draft: false
tags:
- kouign-amann
---

I had the opportunity to visit San Francisco earlier this year. Of course, I
stopped to try the kouign amann.

There are two well-known patisseries for kouign amann in San Francisco. The
first is Arsicault Bakery. There are multiple locations - one near Richmond,
and the other near Civic Center. The second patisserie is b. patisserie in
the Pacific Heights neighborhood.

I went to the Arsicault Bakery's Civic Center location to try their kouign
amann. I left extremely disappointed. While there was a small outer crust on
their kougin amann, most of the rest of the exterior of the pastry was quite
soft - a big red flag for the rest of the kouign amann. Indeed, when I bit into
the pastry, I met what was essentially the consistency of American white bread.
I have never been so disappointed in my life when eating kouign amann.

b. patisserie's kouign amann, however, were a wonder to eat. I went twice! Once
I had the pleasure of eating their black sesame filled kouign amann. The other
time, I tried the plain kouign amann. The outside was crispy from the
caramelized sugar all around the pastry. There was always a great crunch when
biting into it. The inside was moist, and had the consistency of a dense
croissant. While the kouign amann wasn't overly sweet, the amount of sugar in
the pastry overall helped to make it an enjoyable experience.

---
title: More Kouign Amann - Patisserie Chanson
date: 2022-08-12T11:30:00-04:00
draft: false
tags:
- kouign-amann
---

In my last post, I mentioned that Patisserie Chanson wasn't rated even though I
had a kouign amann from there before. Today, I had the opportunity to try the
kouign amann from Patisserie Chanson again.

#### Crust
The crust of the kouign amann at Patisserie Chanson was quite crispy, which was
a big surprise for me. I didn't expect it to be so crispy, to be quite honest.
Unfortunately the crust wasn't sticky to the touch, but it was a little sugary,
which was a big positive. The crust did not glisten. I'd give the crust a 2, or
a 3 at best.

#### Density
This kouign amann was quite dense. The inside was almost similar to a croissant
which isn't uncommon for a lot of the kouign amann that I've had before. It
wasn't layered, which was very disappointing. On the plus side, the kouign amann
felt quite dense, even though the interior was similar to a croissant. I'd rate
the density at a 2.

#### Flavor
The flavor of this kouign amann wasn't the greatest. There were definitely notes
of a palmier in the flavor, which was quite disappointing. I didn't taste any
butter on the kouign amann, but I could clearly taste the sugar crystals. I'd give
the flavor a 2.

#### Moistness
The inside of this kouign amann was surprisingly moist. I was very pleasantly
surprised. I'd rate it a 2.

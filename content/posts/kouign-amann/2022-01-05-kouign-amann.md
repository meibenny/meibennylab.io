---
title: On Kouign Amann
date: 2022-01-05T18:00:00-05:00
draft: false
tags:
- kouign-amann
---

A couple of years ago, I was walking around Montreal and had the good fortune
to encounter the most buttery, fatty, and delicious pastry of my life - the
kouign amann. I even had the luck to try it from Patisserie Au Kouign-Amann!
Could it get any better than that?

To be quite honest, I had no idea what a kouign amann was before I stepped foot
in the patisserie. In fact, I didn't even know to order a kouign amann. I asked
one of the lovely staff members what they recommended from their menu, and they
immediately said a kougin amann. Of course, I couldn't resist a recommendation,
and ordered it immediately in addition to an apple turnover and mocha.

Once I stepped outside of the patisserie, I couldn't resist and eagerly
unwrapped my pastries. On the outside, the kouign amann looked almost like a
slice of pie. It was buttery, flaky, and so sugary, I could swear I saw the
crystals. I took one bite of that flaky, crusty, sweet pastry and I was in
heaven. It was the richest pastry I've had in my life. The fat and sugars coated
my tongue and sent me on an unbelievable flavor adventure. I was hooked.

In the years since, I've tried kouign amann from myriad places, but none beat
the first time I tried it at Patisserie Au Kouign-Amann. An exhuastive listing
of the bakeries where I tried kouign amann:

- Patisserie Au Kouign-Amann, Montreal, QC, Canada
- Patisserie Cannelle, Queens, NY, USA
- Whole Foods Market, White Plains, NY, USA
- Dominique Ansel Bakery, New York, NY, USA
- Dominique Ansel Kitchen, New York, NY, USA
- Mrs. London's Bakery, Saratoga Springs, NY, USA
- Patisserie Chanson, New York, NY, USA
- % Arabica, Brooklyn, NY, USA

My rankings of the kouign amann I've had, with comments:

| Ranking | Patisserie | Comments |
| ------- | ---------- | -------- |
| 1       | Patisserie Au Kouign-Amann | The best kouign amann I've had in my life, ever. All other kouign amann should be compared to the kouign amann at Patisserie Au Kouign-Amann. |
| 2       | Whole Foods Market at White Plains, NY | This is the best kouign amann I've had in the States. The exterior of the pastry was crispy/crunch just as I expected. The butter and sugar was layered on, and the inside was still nice and moist. It compares very favorably to what's offered at Patisserie Au Kouign-Amann, though it's not quite there. |
| 3       | Mrs. London's Bakery in Saratoga Springs, NY | Mrs. London's Bakery doesn't call their kouign amann a kouign amann. They opt to call it a "Brittany". Whatever they like to call it, their version of the kouign amann is quite stellar. It has a crispy, cruncy exterior with a soft interior. I wish they had a little more butter and a little less sugar, but overall it's a fantastic pastry. Considering the paltry offerings in the rest of the States, this is close to perfection. |
| 4       | % Arabica in Brooklyn, NY | % Arabica is a Japanese coffee brand. Surprisingly, their version of the kouign amann is quite good. It has the same crispy/crunchy exterior you expect of all good kouign amann. They seem to use a little less sugar and butter, which is good for my waistline, but unfortunately means that their kouign amann necessarily loses to their richer competitors. Despite its drawbacks, I have no qualms in declaring the kougin amann at % Arabica the best kouign amann in New York City. | 
| 5       | Patisserie Cannelle in Queens, NY | I had the good fortune to try the kougin amann from Patisserie Cannelle while visiting some friends. Their kouign amann is a flat disc shape, reminiscent of the large kouign amann found in Patisserie Au Kouign-Amann, and similar to what you might find in Brittany. The refreshing design is quite different from the other kouign amann found in the states - they usually take on a cupcake form, clearly baked for personal consumption. Patisserie Cannelle's kouign amann is quite delicious. I enjoy the shape, and the flavors, but there's nothing that jumps out at me as either fantastic, or a deal breaker. To be fair, I also ate the kouign amann as the patisserie was closing, so the time the kouign amann was sitting out on display would have definitely affected its texture and flavor profile. Otherwise, it's quite a good option for those in New York City who want to have a taste of what good kouign amann should be. | 
| 7       | Dominque Ansel Bakery and Dominique Ansel Kitchen | I have no words for the kouign amann found at any of Dominique Ansel's properties. They are simply not good representation of kouign amann. For what is allegedly one of the best patisseries in New York City, the kougin amann found at Dominique Ansel are surprisingly bad. The exterior is neither crunchy, crispy, buttery, or sugary. The interior is dry. Overall, the kouign amann found at Dominique Ansel's properties resemble Palmiers in both flavor and texture. Why sell kouign amann at all if the goal is for the pastries to end up as palmiers? The kouign amann found at Dominique Ansel's properties are an affront to kouign amann everywhere. If the only kouign amann you've tried is at Dominique Ansel's properties, I implore you to try kouign amann again at a better patisserie. I'm both sorry, and happy that your first kouign amann experience was, quite possibly, the worst kouign amann experience you've ever had. |
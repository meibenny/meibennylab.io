---
title: interesting places in montreal
date: 2024-03-20T22:42:00-04:00
tags: ["travel", "montreal", "kouign-amann"]
draft: false
---

A list of interesting places to visit in Montreal:

- @matcha, on Rue Rachel. The owner, Nestor, is very kind and knowledgable.
- Au Pied de Cochon, on Avenue Duluth. Chef Martin Picard's food is delicious. And extra.
- Patisserie au Kouign Amann, on Mont Royal Avenue. Try their signature pastry, the kouign amann. Then read my posts on [kouign amann][1]!
- Belvedere Kondiaronk, on Mount Royal. There's a great view of the city from up here.
- Square Victoria - OACI metro station, on the orange line. One of the entrances to this metro station was originally in Paris - one of Hector Guimard's entrances for the Paris Metro. Bonus points for taking a selfie both in Paris, and Montreal.
- Habitat 67, an architectural landmark in Montreal.
- The Biosphere. You're literally inside of a sphere. How cool is that?
- The Biodome, a repurposed Olympic venue turned educational zoo.
- La Banquise, on Rue Rachel, for the world's best poutine.
- Romados, on Rue Rachel, for delicious Portuguese chicken.

[1]: /tags/kouign-amann

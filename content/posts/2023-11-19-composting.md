---
title: composting
date: 2023-11-19T21:50:00-05:00
tags: ["composting"]
draft: false
---

For the past two years, we've been composting our organic waste through [GrowNYC's Compost Program][1].

Composting is our way of creating a more sustainable world. Directing organic waste towards
compost rather than to the landfill helps to complete the nutrient cycle. Organic matter that is
composted helps to ensure that the nutrients and food that help to feed us are returned to the
ecosystem and can be used by other organisms that will, in turn, feed us again.

We first started tracking the amount of organic matter we compost on December 19, 2021. On that day,
we composted 1,481 grams of organic waste.

On November 6, 2022, we composted 2,280 grams of organic matter, bringing our total composted mass
to 51,021 grams.

Today, on November 19, 2023, we composted 1,697 grams, bringing out total composted mass to 101,623 grams!

In a little under two years, we've composted over 100 kilograms of organic waste. I'm happy that
we're doing what we can help make our world a more sustainable place.

[1]: https://www.grownyc.org/compost

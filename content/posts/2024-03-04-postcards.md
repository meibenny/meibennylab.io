---
title: thoughts on postcards
date: 2024-03-04T23:00:00-05:00
tags: ["postcards"]
draft: false
---

For the past couple of years, whenever we visit some place, we try to get
ourselves a souvenir from wherever we visited. Most times, this ends up being
in the form of a humble postcard.

We usually obtain (find, buy, etc.) a postcard from where we're visiting, write
a short, memorable blurb to ourselves, then stamp it, and send it to ourselves
via the postal service.

Part of the fun is not knowing whether or not our postcard will actually make it
all the way back to us.

We've been doing this for a couple of years now, so I wanted to write down and share
my thoughts on how to write a "good" postcard. Mostly this revolves around the format,
preparation on writing the postcard, mailing it, etc. The content that you want to put
in your postcard is entirely a personal decision. I don't want to dictate what you
should or shouldn't write about.

Some of my ideas on how to make your postcard writing journey a success:
- Date your postcard! Very often, you (or your friends) will want to know when you wrote
  and / or sent your postcard.
- Have your address / your friend's address written down beforehand. Nothing is more
  disappointing than not knowing where you mail your postcard.
- Mail your postcard from whereever it is you're visiting. It adds a special touch for
  people to know that your postcard was mailed from "somewhere different".
- Be prepared with stamps. Sometimes the post office won't be open and you'll be stuck
  with a fully written postcard, but no stamp to mail it with! To avoid this situation,
  bring stamps with you!
- If you can, and you have the time, stop by a physical post office location, and ask
  the postal clerk to date stamp your post card. This stamp usually contains the
  name of the post office, its location, and the date of mailing. This makes the
  postcard extra special as the recipient will know (and have physical evidence) of
  the exact post office, and date, where the postcard was mailed
- Sign your postcard! Don't leave yourself, or your friends wondering who sent the
  postcard!

These are some of my thoughts on how to be successful in writing postcards.

I'm always on the lookout for postcard tips and tricks. Let me know your thoughts on
how to be successful sending postcards!
---
title: HOV Lanes in a Post-Roe World 
date: 2022-07-25T18:53:00-04:00
tags: ["dobbs"]
draft: false
---

Shortly before I posted my thoughts on the [questions][1]
we need to ask ourselves after Dobbs, an Texas woman was cited for driving in
the HOV lane whlie pregnant [1][2]. You may have already read about this situation
and have your own thoughts on it. I think we can all agree that the questions I
posted earlier aren't simply academic in nature. They're real questions that
need real answers in a society that wishes to change the definition of personhood.

These aren't contrived scenarios, but real questions that we'll need to answer
together as a country. If abortion is murder, and a fetus is a person, why shouldn't
a pregnant mother be allowed to drive in the HOV lane?

If a pregnant woman isn't allowed to drive in the HOV lane, then can abortion
really end the life of a human being?

[1] "Can You Drive Alone in the H.O.V. Lane if You’re Pregnant? A Post-Roe Quandary." New York Times, 12 July 2022, https://www.nytimes.com/2022/07/12/us/pregnant-woman-hov-lane-roe-wade.html. Accessed 25 July 2022.

[2] "Texas woman says unborn baby should count as a passenger in HOV lane" PBS, 11 July 2022, https://www.pbs.org/newshour/nation/texas-woman-says-unborn-baby-counts-as-passenger-in-hov-lane. Accessed 25 July 2022.

[1]: {{< ref "/posts/post_roe_world/2022-07-13-dobbs-v-jackson.md" >}} 

---
title: Some Further Thoughts on Dobbs
date: 2022-07-13T00:00:00-04:00
tags: ["dobbs"]
draft: false
---

I originally put together some of my thoughts on Dobbs a couple of weeks ago.
I will list them here.

- If a woman is pregnant with twins, can she drive in the HOV lane?
- Can a woman claim her fetus on her taxes?
- Should a fetus count as part of the population?
- If minors can't make their own medical decisions, and parents have total
  control, if assisted suicide finally becomes a thing in the states, does that
  mean abortion is effectively legal?
- Will being pregnant push you over the poverty line so you can get food stamps?
- Can a pregnant woman enter a strip club?
- Can you buy a life insurance policy for a fetus?
- Can you get a social security number for a fetus?
- If a fetus is conceived in a state that considers a fetus a person, is said
  fetus a citizen of the United States?
  - If the fetus's parents aren't citizens of the United States, and they need
    to be deported, should the mom be deported too?
  - If the mom isn't deported (because why would you deport a US citizen),
    should the mom be in jail? 
  - If the mom is in jail, are you falsely imprisoning a US citizen who didn't
    do anything wrong?
- Do pregnant moms need to buy two plane tickets?
- If the mother dies during childbirth due to complications with pregnancy and
  delivery, should the baby be charged with manslaughter / murder?

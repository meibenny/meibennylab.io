---
title: Georgia fetus guidance
date: 2022-08-02T00:00:00-04:00
draft: false
tags: ['dobbs']
---

It's happening. What I've been thinking about is coming true. We're reaching a
point in this country where we are willingly and actively making a decision to
say that unborn babies, fetuses, are human enough to be dependents when filing
taxes. At least, this is the case in Georgia as of August 2, 2022.

NPR reported [today][1] that the Georgia Department of Revenue, Georgia State's
government agency set up to administer Georgia's Tax Laws, released new guidance
that the agency "will recognize any unborn child with a detectable human
heartbeat ... as eligible for the Georgia individual income tax dependent exemption."

We're entering a world where we as a society are beginning to recognize human
life even when the recognized entity is barely more than a clump of cells.

You can read the statement from the Georgia Department of Revenue [here][2].

[1]: https://www.npr.org/2022/08/02/1115204443/georgia-fetus-pregnant-dependent-taxes
[2]: /2022-08-02-georgia-dor-481-guidance.pdf

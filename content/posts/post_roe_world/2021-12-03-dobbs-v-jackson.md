---
title: Thoughts on Dobbs v. Jackson Women's Health Organization
date: 2021-12-03T00:00:00-04:00
tags: ["dobbs"]
draft: False
---

What disturbs me the most about the Supreme Court Case about the Mississippi
Abortion restriction isn't the fact that 15 weeks is a full 10 weeks
(2 and a half months!) before when women currently have the right to an
abortion in the United States.

It's not the fact that rights for an entire half of the country's population
is backsliding to an era before it was acceptable for women to make up a
large proportion of the labor pool.

It's not the fact that the women who will feel the full effects of a missing
10 weeks from their window to receive essential medical care are mostly
underprivileged, indeed underclass, women.

No.

What disturbs me the most about this Supreme Court Case is the fact that the
arguments being made in court are comparing the United States -  what I, and
most probably the world, sees as a bastion of progress, freedom, and individual
rights - to countries such as China and North Korea.

Do we, as a country, look up to China or North Korea? Does any American see
those countries and say to themselves, "Wow, I wish I lived there" or "Why can't
the United States be more like North Korea?". "Why can't the United States be
more like China?" 

I know that there's not a day I don't wake up and say to myself that I wish the
United States of America - with all freedoms and rights that I enjoy - was more
like the People's Republic of China or the Democratic People's Republic of Korea.
